use quaint::error::Error;
use quaint::prelude::{Comparable, Delete, Insert, Queryable, Select, Update};

use community_service_platform::model::user::User;

use super::convert;
use super::CONNECTION;

#[tokio::main]
pub async fn select_all() -> Result<impl Iterator<Item = User>, Error> {
	let query = Select::from_table("user");
	let results = CONNECTION.select(query).await?;
	let users = results.into_iter().map(convert::user_from_row);
	return Ok(users);
}

#[tokio::main]
pub async fn select(identifier: &str) -> Result<User, Box<dyn std::error::Error>> {
	let query = Select::from_table("user")
		.columns(vec!["identifier", "name", "description", "role"])
		.so_that("identifier".equals(identifier));

	let results = CONNECTION.select(query).await?;
	let row = results.into_single()?;
	return Ok(convert::user_from_row(row));
}

#[tokio::main]
pub async fn insert(user: User) -> Result<(), Error> {
	async_insert(user).await
}

pub(super) async fn async_insert(user: User) -> Result<(), Error> {
	let query = Insert::single_into("user")
		.value("identifier", user.identifier)
		.value("password", user.password)
		.value("name", user.name)
		.value("description", user.description)
		.value("role", user.role as i32);

	_ = CONNECTION.insert(query.build()).await?;
	return Ok(());
}

#[tokio::main]
pub async fn update(user: User) -> Result<(), Error> {
	let mut query = Update::table("user")
		.set("name", user.name)
		.set("description", user.description)
		.so_that("identifier".equals(user.identifier));

	if !user.password.is_empty() {
		query = query.set("password", user.password);
	}

	_ = CONNECTION.update(query).await?;
	return Ok(());
}

#[tokio::main]
pub async fn delete(identifier: &str) -> Result<(), Error> {
	let query = Delete::from_table("user").so_that("identifier".equals(identifier));
	CONNECTION.delete(query).await?;
	return Ok(());
}

#[tokio::main]
pub async fn login(user: User) -> Result<User, Box<dyn std::error::Error>> {
	let query = Select::from_table("user")
		.columns(vec!["identifier", "name", "description", "role"])
		.so_that("identifier".equals(user.identifier))
		.and_where("password".equals(user.password));

	let results = CONNECTION.select(query).await?;
	let row = results.into_single()?;
	return Ok(convert::user_from_row(row));
}
