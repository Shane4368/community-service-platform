use quaint::prelude::ResultRow;
use quaint::Value;

use community_service_platform::model::agency::AgencyTask;
use community_service_platform::model::user::{User, UserRole};

pub fn user_from_row(row: ResultRow) -> User {
	let text_value = Value::Text(Some(Default::default()));
	let int32_value = Value::Int32(Some(Default::default()));

	return User {
		identifier: row
			.get("identifier")
			.unwrap_or(&text_value)
			.to_string()
			.unwrap(),

		name: row.get("name").unwrap_or(&text_value).to_string().unwrap(),

		description: row
			.get("description")
			.unwrap_or(&text_value)
			.to_string()
			.unwrap(),

		role: UserRole::from(row.get("role").unwrap_or(&int32_value).as_i32().unwrap()),
		..Default::default()
	};
}

pub fn task_from_row(row: ResultRow) -> AgencyTask {
	let text_value = Value::Text(Some(Default::default()));
	let int32_value = Value::Int32(Some(Default::default()));

	return AgencyTask {
		id: row.get("id").unwrap_or(&int32_value).as_i32().unwrap(),
		name: row.get("name").unwrap_or(&text_value).to_string().unwrap(),

		address: row
			.get("address")
			.unwrap_or(&text_value)
			.to_string()
			.unwrap(),

		description: row
			.get("description")
			.unwrap_or(&text_value)
			.to_string()
			.unwrap(),

		capacity: row
			.get("capacity")
			.unwrap_or(&int32_value)
			.as_i32()
			.unwrap(),

		agency_identifier: row
			.get("agency_identifier")
			.unwrap_or(&text_value)
			.to_string()
			.unwrap(),
	};
}
