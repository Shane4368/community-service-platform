use quaint::error::Error;
use quaint::prelude::{Comparable, Delete, Insert, Queryable, Select};

use community_service_platform::model::agency::AgencyTask;
use community_service_platform::model::user::User;

use super::convert;
use super::CONNECTION;

#[tokio::main]
pub async fn select_all() -> Result<impl Iterator<Item = AgencyTask>, Error> {
	let query = Select::from_table("task");
	let results = CONNECTION.select(query).await?;
	let tasks = results.into_iter().map(convert::task_from_row);
	return Ok(tasks);
}

#[tokio::main]
pub async fn select_all_for(agency: User) -> Result<impl Iterator<Item = AgencyTask>, Error> {
	let query = Select::from_table("task").so_that("agency_identifier".equals(agency.identifier));
	let results = CONNECTION.select(query).await?;
	let tasks = results.into_iter().map(convert::task_from_row);
	return Ok(tasks);
}

#[tokio::main]
pub async fn insert(task: AgencyTask) -> Result<(), Error> {
	let query = Insert::single_into("task")
		.value("id", task.id)
		.value("name", task.name)
		.value("address", task.address)
		.value("description", task.description)
		.value("capacity", task.capacity)
		.value("agency_identifier", task.agency_identifier);

	_ = CONNECTION.insert(query.build()).await?;
	return Ok(());
}

#[tokio::main]
pub async fn delete(id: i32) -> Result<(), Error> {
	let query = Delete::from_table("task").so_that("id".equals(id));
	CONNECTION.delete(query).await?;
	return Ok(());
}
