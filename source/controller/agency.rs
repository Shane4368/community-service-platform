pub mod task;

use quaint::error::Error;
use quaint::prelude::{Comparable, Insert, Queryable, Select};

use community_service_platform::model::agency::Agency;
use community_service_platform::model::user::{User, UserRole};

use super::convert;
use super::{user, CONNECTION};

#[tokio::main]
pub async fn select_all() -> Result<impl Iterator<Item = User>, Error> {
	let query = Select::from_table("user").so_that("role".equals(UserRole::Agency as i32));
	let results = CONNECTION.select(query).await?;
	let users = results.into_iter().map(convert::user_from_row);
	return Ok(users);
}

#[tokio::main]
pub async fn insert(agency: Agency) -> Result<(), Error> {
	let agency_user = agency.user().clone();

	let query = Insert::single_into("agency")
		.value("identifier", agency_user.identifier.clone())
		.value("address", agency.address);

	user::async_insert(agency_user).await?;
	_ = CONNECTION.insert(query.build()).await?;
	return Ok(());
}
