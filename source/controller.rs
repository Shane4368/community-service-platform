pub mod agency;
mod convert;
pub mod user;

use std::fs;

use once_cell::sync::Lazy;
use quaint::error::Error;
use quaint::prelude::Queryable;
use quaint::single::Quaint;

use crate::view::home;

static CONNECTION: Lazy<Quaint> = Lazy::new(|| create_connection().unwrap());

pub fn view() {
	_ = Lazy::force(&CONNECTION);
	home::show();
}

#[tokio::main]
async fn create_connection() -> Result<Quaint, Error> {
	let query = fs::read_to_string("create_tables.sql").unwrap();
	let connection = Quaint::new("file:database.db").await?;
	_ = connection.raw_cmd(&query).await?;
	return Ok(connection);
}
