#[derive(Debug, Default, Clone)]
pub struct User {
	pub identifier: String,
	pub password: String,
	pub name: String,
	pub description: String,
	pub settings: UserSettings,
	pub role: UserRole,
	pub emails: Vec<String>,
	pub telephones: Vec<String>,
}

#[derive(Debug, Default, Clone)]
pub struct UserSettings {
	pub share_info: bool,
}

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub enum UserRole {
	#[default]
	None,
	Agency,
	Lecturer,
	Student,
	Volunteer,
}

impl From<i32> for UserRole {
	fn from(value: i32) -> Self {
		match value {
			1 => Self::Agency,
			2 => Self::Lecturer,
			3 => Self::Student,
			4 => Self::Volunteer,
			_ => Self::None,
		}
	}
}
