use super::user::User;

#[derive(Debug, Default)]
pub struct Agency {
	pub address: String,
	pub tasks: Vec<AgencyTask>,
	user: User,
}

#[derive(Debug, Default, Clone)]
pub struct AgencyTask {
	pub id: i32,
	pub name: String,
	pub address: String,
	pub description: String,
	pub capacity: i32,
	pub agency_identifier: String,
}

impl Agency {
	pub fn user(&self) -> &User {
		&self.user
	}

	pub fn user_mut(&mut self) -> &mut User {
		&mut self.user
	}
}

impl From<User> for Agency {
	fn from(value: User) -> Self {
		Self {
			user: value,
			..Default::default()
		}
	}
}
