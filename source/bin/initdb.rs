use std::fs;

use fake::faker::address::en::StreetName;
use fake::faker::boolean::en::Boolean;
use fake::faker::company::en::CompanyName;
use fake::faker::internet::en::{FreeEmailProvider, Password};
use fake::faker::lorem::en::{Paragraph, Words};
use fake::faker::name::en::Name;
use fake::faker::number::en::Digit;
use fake::faker::phone_number::en::{CellNumber, PhoneNumber};
use fake::Fake;
use quaint::error::Error;
use quaint::prelude::{Insert, Queryable};
use quaint::single::Quaint;
use quaint::Value;
use rand::Rng;

use community_service_platform::model::agency::{Agency, AgencyTask};
use community_service_platform::model::user::{User, UserRole, UserSettings};

#[tokio::main]
async fn create_connection() -> Result<Quaint, Error> {
	let query = fs::read_to_string("create_tables.sql").unwrap();
	let connection = Quaint::new("file:database.db").await?;
	_ = connection.raw_cmd(&query).await?;
	return Ok(connection);
}

async fn async_insert_emails(
	connection: &Quaint,
	emails: Vec<(String, String)>,
) -> Result<(), Error> {
	let mut query = Insert::multi_into("email", vec!["identifier", "value"]);

	for email in emails {
		query = query.values((email.0, email.1));
	}

	_ = connection.insert(query.build()).await?;
	return Ok(());
}

async fn async_insert_telephones(
	connection: &Quaint,
	telephones: Vec<(String, String)>,
) -> Result<(), Error> {
	let mut query = Insert::multi_into("telephone", vec!["identifier", "value"]);

	for telephone in telephones {
		query = query.values((telephone.0, telephone.1));
	}

	_ = connection.insert(query.build()).await?;
	return Ok(());
}

async fn async_insert_user_settings(
	connection: &Quaint,
	user_settings: Vec<(String, UserSettings)>,
) -> Result<(), Error> {
	let mut query = Insert::multi_into("user_settings", vec!["identifier", "share_info"]);

	for (identifier, settings) in user_settings {
		query = query.values((identifier, settings.share_info));
	}

	_ = connection.insert(query.build()).await?;
	return Ok(());
}

async fn async_insert_tasks(connection: &Quaint, tasks: Vec<AgencyTask>) -> Result<(), Error> {
	let mut query = Insert::multi_into(
		"task",
		vec![
			"id",
			"name",
			"address",
			"description",
			"capacity",
			"agency_identifier",
		],
	);

	for task in tasks {
		query = query.values(vec![
			Value::from(task.id),
			Value::from(task.name),
			Value::from(task.address),
			Value::from(task.description),
			Value::from(task.capacity),
			Value::from(task.agency_identifier),
		]);
	}

	_ = connection.insert(query.build()).await?;
	return Ok(());
}

async fn async_insert_users(connection: &Quaint, users: Vec<User>) -> Result<(), Error> {
	let mut query = Insert::multi_into(
		"user",
		vec!["identifier", "password", "name", "description", "role"],
	);

	let mut user_settings = Vec::new();
	let mut emails = Vec::new();
	let mut telephones = Vec::new();

	for user in users {
		query = query.values((
			user.identifier.clone(),
			user.password.clone(),
			user.name.clone(),
			user.description.clone(),
			user.role.clone() as i32,
		));

		user_settings.push((user.identifier.clone(), user.settings.clone()));

		user.emails
			.iter()
			.for_each(|email| emails.push((user.identifier.clone(), email.clone())));

		user.telephones
			.iter()
			.for_each(|telephone| telephones.push((user.identifier.clone(), telephone.clone())));
	}

	_ = connection.insert(query.build()).await?;

	async_insert_user_settings(connection, user_settings).await?;
	async_insert_emails(connection, emails).await?;
	async_insert_telephones(connection, telephones).await?;

	return Ok(());
}

#[tokio::main]
async fn insert_users(connection: &Quaint, users: Vec<User>) -> Result<(), Error> {
	async_insert_users(connection, users).await
}

#[tokio::main]
async fn insert_agencies(connection: &Quaint, agencies: Vec<Agency>) -> Result<(), Error> {
	let mut query = Insert::multi_into("agency", vec!["identifier", "address"]);

	let mut users = Vec::new();
	let mut agency_tasks = Vec::new();

	for agency in agencies {
		let user = agency.user();

		query = query.values((user.identifier.clone(), agency.address.clone()));

		users.push(user.clone());

		agency
			.tasks
			.iter()
			.for_each(|task| agency_tasks.push(task.clone()));
	}

	async_insert_users(connection, users).await?;
	async_insert_tasks(connection, agency_tasks).await?;

	_ = connection.insert(query.build()).await?;
	return Ok(());
}

fn create_tasks(agency_identifier: &str, max: i32) -> Vec<AgencyTask> {
	static mut COUNT: i32 = 1;

	let mut tasks = Vec::new();
	let mut rng = rand::thread_rng();

	unsafe {
		let new_count = COUNT + max;

		for id in COUNT..new_count {
			let task = AgencyTask {
				id,
				name: Words(2..6).fake::<Vec<String>>().join(" "),
				address: StreetName().fake(),
				description: Paragraph(2..4).fake(),
				capacity: rng.gen_range(1..=15),
				agency_identifier: agency_identifier.to_owned(),
			};

			tasks.push(task);
		}

		COUNT = new_count;
	}

	return tasks;
}

fn create_agencies(max: i32) -> Vec<Agency> {
	let mut agencies = Vec::new();

	for _ in 0..max {
		let mut user = User {
			password: Password(8..9).fake(),
			name: CompanyName().fake(),
			description: Paragraph(2..4).fake(),
			role: UserRole::Agency,
			telephones: vec![PhoneNumber().fake()],
			..Default::default()
		};

		user.identifier = user.name.replace(' ', ".").to_lowercase();

		user.emails.push(format!(
			"{}@{}",
			user.identifier,
			FreeEmailProvider().fake::<&str>()
		));

		let mut agency = Agency::from(user);

		agency.address = StreetName().fake();

		agency.tasks = create_tasks(
			&agency.user().identifier,
			Digit().fake::<&str>().parse().unwrap(),
		);

		agencies.push(agency);
	}

	return agencies;
}

fn create_volunteers(max: i32) -> Vec<User> {
	let mut users = Vec::new();

	for _ in 0..max {
		let mut user = User {
			password: Password(8..9).fake(),
			name: Name().fake(),
			description: Paragraph(2..4).fake(),
			role: UserRole::Volunteer,
			settings: UserSettings {
				share_info: Boolean(50).fake(),
			},
			telephones: vec![CellNumber().fake()],
			..Default::default()
		};

		user.identifier = user.name.replace(' ', ".").to_lowercase();

		let email = format!("{}@{}", user.identifier, FreeEmailProvider().fake::<&str>());

		user.emails.push(email);
		users.push(user);
	}

	return users;
}

fn create_students(max: i32) -> Vec<User> {
	let mut users = Vec::new();

	for _ in 0..max {
		let mut user = User {
			identifier: (1_000_000..3_000_000).fake::<i32>().to_string(),
			password: Password(8..9).fake(),
			name: Name().fake(),
			description: Paragraph(2..4).fake(),
			role: UserRole::Student,
			settings: UserSettings {
				share_info: Boolean(50).fake(),
			},
			telephones: vec![CellNumber().fake()],
			..Default::default()
		};

		let email = format!(
			"{}@{}",
			user.name.replace(' ', ".").to_lowercase(),
			FreeEmailProvider().fake::<&str>()
		);

		user.emails.push(email);
		users.push(user);
	}

	return users;
}

fn create_lecturers(max: i32) -> Vec<User> {
	let mut users = Vec::new();
	let mut rng = rand::thread_rng();

	for _ in 0..max {
		let mut user = User {
			password: Password(8..9).fake(),
			name: Name().fake(),
			description: Paragraph(2..4).fake(),
			role: UserRole::Lecturer,
			settings: UserSettings {
				share_info: Boolean(50).fake(),
			},
			telephones: vec![CellNumber().fake()],
			..Default::default()
		};

		user.identifier = user.name.replace(' ', ".").to_lowercase();

		let email_count = rng.gen_range(1..=2);

		for _ in 0..email_count {
			let email = format!("{}@{}", user.identifier, FreeEmailProvider().fake::<&str>());
			user.emails.push(email);
		}

		user.emails.dedup();
		users.push(user);
	}

	return users;
}

fn main() -> Result<(), Error> {
	let connection = create_connection()?;

	let lecturers = create_lecturers(3);
	let students = create_students(10);
	let agencies = create_agencies(10);
	let volunteers = create_volunteers(5);

	insert_users(&connection, lecturers)?;
	insert_users(&connection, students)?;
	insert_users(&connection, volunteers)?;
	insert_agencies(&connection, agencies)?;

	return Ok(());
}
