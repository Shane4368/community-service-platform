pub mod home;

mod agency;
mod lecturer;
mod student;
mod volunteer;

mod console {
	use std::io::{self, Write};
	use std::process::Command;

	use super::constant::escapecode;

	/// Prints to the standard output, and flush the standard output stream.
	pub fn print(text: &str) {
		print!("{text}");
		_ = io::stdout().flush();
	}

	/// Prints to the standard error, with a newline.
	pub fn eprintln(text: &str) {
		eprintln!(
			"{}-- ERROR: {text} --{}\n",
			escapecode::RED,
			escapecode::RESET
		);
	}

	pub fn read_i32() -> i32 {
		let mut input = String::with_capacity(4);
		_ = io::stdin().read_line(&mut input);
		return input.trim_end().parse::<i32>().unwrap();
	}

	pub fn read_line() -> String {
		let mut input = String::new();
		_ = io::stdin().read_line(&mut input);
		return input.trim_end().to_string();
	}

	pub fn prompt_for_enter_keypress() {
		print(&format!(
			"{}Press 'Enter' to continue.{}",
			escapecode::GREEN,
			escapecode::RESET
		));

		let mut input = String::new();
		_ = io::stdin().read_line(&mut input);
	}

	pub fn clear_screen() {
		if cfg!(windows) {
			_ = Command::new("powershell.exe").arg("Clear-Host").status();
			return;
		}

		print!("\x1b[2J\x1b[H");
	}
}

mod constant {
	pub mod escapecode {
		pub const RESET: &str = "\x1b[0m";
		pub const UNDERLINE: &str = "\x1b[4m";

		#[allow(dead_code)]
		pub const BLACK: &str = "\x1b[1;30m";
		pub const RED: &str = "\x1b[1;31m";
		pub const GREEN: &str = "\x1b[1;32m";
	}
}

use once_cell::sync::Lazy;

use community_service_platform::model::user::User;

static mut CURRENT_USER: Lazy<User> = Lazy::new(User::default);

fn get_current_user() -> &'static User {
	unsafe { Lazy::force(&CURRENT_USER) }
}

fn set_current_user(user: User) {
	unsafe {
		*CURRENT_USER = user;
	}
}

fn print_title(title: &str) {
	println!("--- [ {title} | Community Service Platform ] ---\n");
}
