use community_service_platform::model::agency::Agency;
use community_service_platform::model::user::{User, UserRole};

use crate::controller;

use super::agency;
use super::console;
use super::lecturer;
use super::student;
use super::volunteer;

pub fn show() {
	loop {
		console::print(concat!(
			"------- [ Community Service Platform ] -------\n\n",
			"1. Log in\n",
			"2. Create a new profile\n",
			"0. Exit\n\n",
			"Enter option: "
		));

		let option = console::read_i32();

		console::clear_screen();

		match option {
			1 => show_screen1("Log in"),
			2 => show_screen2("Create new profile"),
			0 => return,
			_ => (),
		}

		console::prompt_for_enter_keypress();
		console::clear_screen();
	}
}

fn show_screen1(title: &str) {
	super::print_title(title);

	let mut user = User::default();

	console::print("Enter username/ID: ");
	user.identifier = console::read_line();

	console::print("Enter password: ");
	user.password = console::read_line();

	let result = controller::user::login(user);

	if result.is_err() {
		console::eprintln("Incorrect username/ID or password");
		return;
	}

	user = result.unwrap();

	println!("You are now logged in as {}.", user.name);

	console::prompt_for_enter_keypress();
	console::clear_screen();
	super::set_current_user(user.clone());

	match user.role {
		UserRole::Agency => agency::show(),
		UserRole::Lecturer => lecturer::show(),
		UserRole::Student => student::show(),
		UserRole::Volunteer => volunteer::show(),
		UserRole::None => (),
	}
}

fn show_screen2(title: &str) {
	super::print_title(title);

	console::print(concat!(
		"1. Agency\n",
		"2. Lecturer\n",
		"3. Student\n",
		"4. Volunteer\n\n",
		"Enter profile type: "
	));

	let role = UserRole::from(console::read_i32());

	if role == UserRole::None {
		console::eprintln("Profile type is not valid");
		return;
	}

	let mut user = User {
		role,
		..Default::default()
	};

	console::print("Enter name: ");
	user.name = console::read_line();

	console::print("Enter username/ID: ");
	user.identifier = console::read_line();

	console::print("Enter description: ");
	user.description = console::read_line();

	if user.role == UserRole::Agency {
		let mut agency = Agency::from(user);

		console::print("Enter address: ");
		agency.address = console::read_line();

		agency.user_mut().password = password_prompt();

		let result = controller::agency::insert(agency);

		if let Err(error) = result {
			console::eprintln(&error.to_string());
		}

		return;
	}

	user.password = password_prompt();

	let result = controller::user::insert(user);

	if let Err(error) = result {
		console::eprintln(&error.to_string());
	}

	fn password_prompt() -> String {
		console::print("Enter password: ");
		return console::read_line();
	}
}
