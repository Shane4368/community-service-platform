use community_service_platform::model::agency::AgencyTask;
use community_service_platform::model::user::UserRole;

use crate::controller;

use super::console;

pub fn show() {
	loop {
		super::print_title("Agency");
		console::print(concat!(
			"1. View list of volunteers\n",
			"2. View list of accepted volunteers\n",
			"3. View list of past volunteers\n",
			"4. View voluntary jobs\n",
			"5. Post new voluntary job\n",
			"6. Edit your profile\n",
			"0. Log out\n\n",
			"Enter option: "
		));

		let option = console::read_i32();

		console::clear_screen();

		match option {
			1 => show_screen1("Volunteers"),
			4 => show_screen4("Voluntary Jobs"),
			5 => show_screen5("Create voluntary job"),
			0 => return,
			_ => (),
		}

		console::prompt_for_enter_keypress();
		console::clear_screen();
	}
}

fn show_screen1(title: &str) {
	super::print_title(title);

	let users = controller::user::select_all()
		.unwrap()
		.filter(|x| x.role == UserRole::Student || x.role == UserRole::Volunteer);

	for user in users {
		println!("{}", user.name);
	}
}

fn show_screen4(title: &str) {
	super::print_title(title);

	let user = super::get_current_user().clone();
	let tasks = controller::agency::task::select_all_for(user).unwrap();

	for task in tasks {
		let task_body = format!("Task: {}\n", task.name)
			+ &format!("Address: {}\n", task.address)
			+ &format!("Description: {}\n", task.description)
			+ &format!("Capacity: {}\n", task.capacity);

		println!("{task_body}");
	}
}

fn show_screen5(title: &str) {
	super::print_title(title);

	let user = super::get_current_user();
	let mut task = AgencyTask {
		agency_identifier: user.identifier.clone(),
		..Default::default()
	};

	console::print("Enter task ID: ");
	task.id = console::read_i32();

	console::print("Enter task name: ");
	task.name = console::read_line();

	console::print("Enter task address: ");
	task.address = console::read_line();

	console::print("Enter task description: ");
	task.description = console::read_line();

	console::print("Enter task capacity: ");
	task.capacity = console::read_i32();

	let result = controller::agency::task::insert(task);

	if let Err(error) = result {
		console::eprintln(&error.to_string());
	}
}
