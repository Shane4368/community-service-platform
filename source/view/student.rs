use community_service_platform::model::user::User;

use crate::controller;

use super::console;
use super::constant::escapecode;

pub fn show() {
	loop {
		super::print_title("Student");
		console::print(concat!(
			"1. View list of agencies\n",
			"2. View agencies currently facilitating students\n",
			"3. Search for profile\n",
			"4. Edit your profile\n",
			"0. Log out\n\n",
			"Enter option: "
		));

		let option = console::read_i32();

		console::clear_screen();

		match option {
			1 => show_screen1("Agencies"),
			2 => show_screen2("Voluntary Jobs"),
			3 => show_screen3("Search profile"),
			4 => show_screen4("Edit profile"),
			0 => return,
			_ => (),
		}

		console::prompt_for_enter_keypress();
		console::clear_screen();
	}
}

fn show_screen1(title: &str) {
	super::print_title(title);

	let result = controller::agency::select_all();

	if let Err(error) = result {
		console::eprintln(&error.to_string());
		return;
	}

	for user in result.unwrap() {
		println!("{}", user.name);
	}
}

fn show_screen2(title: &str) {
	super::print_title(title);

	let result = controller::agency::task::select_all();

	if let Err(error) = result {
		console::eprintln(&error.to_string());
		return;
	}

	for task in result.unwrap() {
		let task_body = format!("\tTask: {}\n", task.name)
			+ &format!("\tAddress: {}\n", task.address)
			+ &format!("\tDescription: {}\n", task.description)
			+ &format!("\tCapacity: {}\n", task.capacity);

		println!("{} {{\n{}}}\n", task.agency_identifier, task_body);
	}
}

fn show_screen3(title: &str) {
	super::print_title(title);

	let mut user = User::default();

	console::print("Enter username/ID of profile: ");
	user.identifier = console::read_line();

	let result = controller::user::select(&user.identifier);

	if result.is_err() {
		console::eprintln(&format!("Could not find {}", user.identifier));
		return;
	}

	user = result.unwrap();

	println!(
		"{3}{}{4}\n{}\n\nshare_volunteering_info={}\n",
		user.name,
		user.description,
		user.settings.share_info,
		escapecode::UNDERLINE,
		escapecode::RESET
	);
}

fn show_screen4(title: &str) {
	super::print_title(title);

	let mut user = super::get_current_user().clone();

	println!(
		"{3}{}{4}\n{}\n\nshare_volunteering_info={}\n",
		user.name,
		user.description,
		user.settings.share_info,
		escapecode::UNDERLINE,
		escapecode::RESET
	);

	console::print("Enter new name: ");
	let name = console::read_line();

	console::print("Enter new description: ");
	let description = console::read_line();

	console::print("Enter new password: ");
	let password = console::read_line();

	console::print("Share voluntering information? (y/n): ");
	let share_info = console::read_line();

	if !name.is_empty() {
		user.name = name;
	}

	if !description.is_empty() {
		user.description = description;
	}

	if !password.is_empty() {
		user.password = password;
	}

	if !share_info.is_empty() {
		user.settings.share_info = share_info.starts_with('y');
	}

	controller::user::update(user.clone()).unwrap();
	super::set_current_user(user);
}
