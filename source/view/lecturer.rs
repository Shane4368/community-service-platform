use crate::controller;

use super::console;

pub fn show() {
	loop {
		super::print_title("Lecturer");
		console::print(concat!(
			"1. View list of agencies\n",
			"2. View agencies currently facilitating students\n",
			"3. View your students\n",
			"4. Search for profile\n",
			"5. Edit your profile\n",
			"0. Log out\n\n",
			"Enter option: "
		));

		let option = console::read_i32();

		console::clear_screen();

		match option {
			1 => show_screen1("Agencies"),
			0 => return,
			_ => (),
		}

		console::prompt_for_enter_keypress();
		console::clear_screen();
	}
}

fn show_screen1(title: &str) {
	super::print_title(title);

	let result = controller::agency::select_all();

	if let Err(error) = result {
		console::eprintln(&error.to_string());
		return;
	}

	for user in result.unwrap() {
		println!("{}", user.name);
	}
}
