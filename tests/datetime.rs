use time::format_description;
use time::format_description::well_known::{Iso8601, Rfc3339};
use time::macros::datetime;
use time::PrimitiveDateTime;

fn datetime_from_iso8601_string(text: &str) -> PrimitiveDateTime {
	PrimitiveDateTime::parse(text, &Iso8601::PARSING).unwrap()
}

#[test]
fn test_datetime_from_iso8601_string() {
	assert_eq!(
		datetime!(2023-05-10 00:00),
		datetime_from_iso8601_string("2023-05-10T00:00")
	);

	assert_eq!(
		datetime!(2023-05-10 1:30),
		datetime_from_iso8601_string("2023-05-10T01:30")
	);

	assert_eq!(
		datetime!(2023-05-10 18:30),
		datetime_from_iso8601_string("2023-05-10T18:30")
	);
}

fn datetime_from_rfc3339_string(text: &str) -> PrimitiveDateTime {
	PrimitiveDateTime::parse(text, &Rfc3339).unwrap()
}

#[test]
fn test_datetime_from_rfc3339_string() {
	assert_eq!(
		datetime!(2023-05-10 00:00),
		datetime_from_rfc3339_string("2023-05-10T00:00:00Z")
	);

	assert_eq!(
		datetime!(2023-05-10 1:30),
		datetime_from_rfc3339_string("2023-05-10T01:30:00Z")
	);

	assert_eq!(
		datetime!(2023-05-10 18:30),
		datetime_from_rfc3339_string("2023-05-10T18:30:00+00:00")
	);
}

fn datetime_from_custom_24hformat_string(text: &str) -> PrimitiveDateTime {
	let custom_format =
		format_description::parse("[year]-[month]-[day] [hour padding:none]:[minute]").unwrap();
	return PrimitiveDateTime::parse(text, &custom_format).unwrap();
}

#[test]
fn test_datetime_from_custom_24hformat_string() {
	assert_eq!(
		datetime!(2023-05-10 00:00),
		datetime_from_custom_24hformat_string("2023-05-10 0:00")
	);

	assert_eq!(
		datetime!(2023-05-10 1:30),
		datetime_from_custom_24hformat_string("2023-05-10 1:30")
	);

	assert_eq!(
		datetime!(2023-05-10 18:30),
		datetime_from_custom_24hformat_string("2023-05-10 18:30")
	);
}

fn datetime_from_custom_12hformat_string(text: &str) -> PrimitiveDateTime {
	let custom_format = format_description::parse(
		"[year]-[month]-[day] [hour padding:none repr:12]:[minute] [period]",
	)
	.unwrap();

	return PrimitiveDateTime::parse(text, &custom_format).unwrap();
}

#[test]
fn test_datetime_from_custom_12hformat_string() {
	assert_eq!(
		datetime!(2023-05-10 00:00),
		datetime_from_custom_12hformat_string("2023-05-10 12:00 AM")
	);

	assert_eq!(
		datetime!(2023-05-10 1:30),
		datetime_from_custom_12hformat_string("2023-05-10 1:30 AM")
	);

	assert_eq!(
		datetime!(2023-05-10 18:30),
		datetime_from_custom_12hformat_string("2023-05-10 6:30 PM")
	);
}
