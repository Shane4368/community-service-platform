# Community Service Platform

This project was proposed as a web application; however, I built it in
Rust to learn more about the language and its toolchain.

## Getting Started with Rust

```shell
# initialize without version control system
cargo init --vcs none

# format the current package
cargo fmt

# test the specified integration test in tests/
cargo test --test name

# run the default binary
cargo run
```

## Coding Conventions

- Use ***PascalCase*** for struct and trait names
- Use ***flatcase*** or ***snake_case*** for field names
- Use ***flatcase*** or ***snake_case*** for method and function names
- Use ***flatcase*** or ***snake_case*** for variable names
- Use ***flatcase*** or ***snake_case*** for module and crate names
- Use ***kebab-case*** for package names
- Use ***MACRO_CASE*** for static and constant items

> **See more**
>
> Rust API Guidelines: https://rust-lang.github.io/api-guidelines/
>
> rust-analyzer Code Style: https://github.com/rust-lang/rust-analyzer/blob/master/docs/dev/style.md

### Conversions

- `&T` to `T` should use `.clone()`
- `&[T]` to `Vec<T>` should use `.to_owned()`
- `&str` to `String` should use `.to_owned()`
